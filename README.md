# Mage-LaTeX

This is an extension for Magento that generates PDF packing slips and
invoices using LaTeX. It is based heavily on an extension written by
Michael Müller (http://micha.elmueller.net/) in 2010.

With this extension, you can use your own LaTeX template to create
nice-looking PDFs with your corporate design, correct hyphenation and
all the goodies of TeX typesetting.

## Motivation

The PDFs created by Magento look awful, especially the packing slips. As
I'd just created letterheads for our company using Emacs, Org-Mode, and
Koma-Script, I was looking for a way to use this for Magento, too. 

I would welcome any contributions to make this extension more generic.
At the moment, this is »works for me« software that you can adapt to
make it work for you.

## Installation

This is my first Magento extension, so use with extreme caution. Copy
the file tree to your Magento installation, adjust the templates, and
pick the correct Invoice.php out of the two choices (default or with
Novalnet Payment module installed, which extends the same class this
extension extends.)

Also, you need to comment out the rewrite directive in Novalnet's
Payment module's config.xml pertaining to order_pdf_invoice. And if you
don't use this module, remove the »depends« directive in
app/etc/Mederle_Latex.


## Templates

The extension comes with two LaTeX templates: 

* German invoice (template.tex)
* German packing slip (shipment.tex)

You can modify the templates by copying them into /media/latex/,
change /app/code/local/Mederle/Latex/etc/config.php to use your template.

For example add this code to the config.php to use 
the file /media/latex/my-invoice.tex as invoice template.

	$config[1] = array(
		'filename' => 'my-invoice',
		'currency' => '\textup{\euro}',
		'date' => 'd.m.Y'
	);
