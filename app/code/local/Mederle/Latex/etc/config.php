<?php
/**
 * Magento on LaTeX Extension
 *
 * This is the config file.
 *
 * @copyright  Copyright (c) 2015 Wolfgang Mederle (http://www.mederle.de/beratung/)
 * @license    LGPL
 */

//////// ******************** DONT TOUCH - STARTING ************************ ////////

$dateFields = array('created_at');
$priceFields = array('base_total', 'subtotal', 'grand_total', 'original_price', 'row_total', 
'tax_amount', 'price_incl_tax', 'row_price_incl_tax');

$config = array();
$config['standard'] = array(
	'currency' => '\textup{\euro}',
	'date' => 'd.m.Y',
	'dateFields' => $dateFields,
	'priceFields' => $priceFields	
);
//////// ******************** DONT TOUCH - ENDING   ************************ ////////




/*	****** EXAMPLE CONFIG  ******** 
	The files you specifiy here have to be located at
		media/latex/
*/

// note that StoreID is no longer used here, everything's hardcoded. Works for me!
// config for invoice
$config[1] = array(
	'filename' => 'template',
	'currency' => '\textup{\euro}',
	'date' => 'd.m.Y'
);

// config for shipment
$config[2] = array(
	'filename' => 'shipment',
	'currency' => '\textup{\euro}',
	'date' => 'd.m.Y'
);
